##############
* truebackup *
##############

truebackup.sh - скрыпт бэкапу дадзеных. 
Па-змоўчаньні праграма робіць копію ўсёй сыстэмы й баз дадзеных MySQL/PostgreSQL у тэчку /mnt/backup/server пад назвай кшталту "2013-02-28-1318": Год-Месяц-Дзень-ГадзінаХвіліна.



####################
* truebackup_clear *
####################

truebackup_clear.sh - скрыпт выдаленьня старых бэкапаў па заданым крытэрам. 
Па-змоўчаньні выдаляюцца бэкапы старэйшыя за 7 дзён.

###########
* Crontab *
###########

Скрыпты можна пакласьці ў /opt кампутара й дадаць у crontab (`crontab -e`) наступныя радкі:

@midnight       /opt/truebackup/truebackup.sh
@midnight       /opt/truebackup/truebackup_clear.sh

Такім чынам бэкапы будуць рабіцца кожны дзень у 00:00 ночы й у /mnt/backup/server будуць захоўвацца копіі за апошні тыдзень.

NB! Пры аткываванай пераменнай SYSTEM_ENABLE запуск скрыптоў павінен адбывацца ад імя root.

####

Афіцыйная старонка: 
https://bitbucket.org/soko1/truebackup/src

Git:
$ git clone https://soko1@bitbucket.org/soko1/truebackup.git