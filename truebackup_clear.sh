#!/bin/sh

# (C) Aliaksei Sakalou <nullbsd@gmail.com>
#
# script name: truebackup_clear.sh
#
# desc: This script to remove old backup systems and databases
#
# git repo: 
# $ git clone https://git@bitbucket.org/soko1/useful.git

# Global
BACKUP_OUT=/mnt/backup/server
TIME_TO_DIE=7 # Days
SLEEP="sleep"
SLEEP_TIME=7; # Seconds

if [ ! -d $BACKUP_OUT ]; then
	echo \'$BACKUP_OUT\' not found!
	echo "Exiting..."
	exit
fi

echo "Clear old backup... (wait $SLEEP_TIME secounds)"
echo "(To cancel, press: Ctrl+c)"
$SLEEP $SLEEP_TIME
find $BACKUP_OUT -maxdepth 1 -ctime +$TIME_TO_DIE -type d -exec rm -frv {} \;