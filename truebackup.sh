#!/bin/sh

# (C) Aliaksei Sakalou <nullbsd@gmail.com>
#
# script name: truebackup.sh
#
# desc: This script to create backup systems and databases
#
# git repo: 
# $ git clone https://git@bitbucket.org/soko1/truebackup.git

# Global
BACKUP_OUT=/mnt/backup/server
BACKUP_IN=/
TIME=`date +%F-%H%M`
RSYNC="/usr/bin/rsync -axv"
SLEEP="sleep"
SLEEP_TIME=7;

# make a backup of the system?
SYSTEM_ENABLE=1

# make a backup of the DBMS?
MYSQL_ENABLE=1
PGSQL_ENABLE=1

#if PGSQL_ENABLE or MYSQL_ENABLE set 1
## MySQL
MYSQL_USER="root"
MYSQL_PASSWD=""
MYSQLDB="--all-databases"
MYSQLDUMP="/usr/bin/mysqldump"
MYSQLDUMP_NAME="mysql.gz"

## PostgreSQL
#PGSQL_USER="postgres"
#PGSQL_PASSWD=""
#PGSQLDB=""
PGSQLDUMP="su - postgres -c /usr/bin/pg_dumpall"
PGSQLDUMP_NAME="pgsql.gz"
##

if [ ! -d $BACKUP_OUT/$TIME ]; then
	mkdir -p $BACKUP_OUT/$TIME
fi

if [ $SYSTEM_ENABLE = 1 ]; then
	echo "Backup system...(wait $SLEEP_TIME second)"
	echo "(To cancel, press: Ctrl+c)"
	$SLEEP $SLEEP_TIME
	$RSYNC $BACKUP_IN $BACKUP_OUT/$TIME
fi

if [ $MYSQL_ENABLE = 1 ]; then
	echo "Backup MYSQL DATABASES...(wait $SLEEP_TIME second)"
	echo "(To cancel, press: Ctrl+c)"
	$SLEEP $SLEEP_TIME
	$MYSQLDUMP $MYSQLDB -u $MYSQL_USER -p$MYSQL_PASSWD | gzip -f -8 - >$BACKUP_OUT/$TIME/$MYSQLDUMP_NAME
fi

if [ $PGSQL_ENABLE = 1 ]; then
	echo "Backup PostgreSQL DATABASES...(wait $SLEEP_TIME second)"
	echo "(To cancel, press: Ctrl+c)"
	$SLEEP $SLEEP_TIME
	$PGSQLDUMP | gzip -f -8 - > $BACKUP_OUT/$TIME/$PGSQLDUMP_NAME
fi
